#include "UdpSocket.h"
#include <stdio.h>
#include <errno.h>
#include <strings.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> 
#include <stdlib.h>
#include <assert.h>

UdpSocket::UdpSocket(unsigned int bufferSize)
{
	m_bufSize = bufferSize;
	m_buffer = new char;
	m_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (m_sock < 0)
	{
		perror("UdpSocket:ERROR: Failed to initiate socket.");
		printf("UdpSocket:INFO: The socket needs to be rebuilt.\n");
	}
}

bool UdpSocket::Bind(const char *addr, int port)
{
	memset(&m_fromAddr, 0, sizeof(m_fromAddr)); /* Clear out the address. */
	m_fromAddr.sin_family = AF_INET;
	m_fromAddr.sin_port = htons(port);
	if (inet_pton(AF_INET, addr, &m_fromAddr.sin_addr.s_addr ) < 1)
	{
		perror("UdpSocket:ERROR:bind(): Failed to resolve address.");
		printf("UdpSocket:INFO:bind(): Check if the address exists.\n");
		return false;
	}
	if (bind(m_sock, (sockaddr *)&m_fromAddr, sizeof(m_fromAddr)) == -1)
	{
		perror("UdpSocket:ERROR:Bind(): Failed to bind.");
		printf("UdpSocket:INFO:Bind(): Socket needs to re-bind.\n");
		return false;
	}
	return true; /* Successfully binded socket. */
}

int UdpSocket::Receive()
{
	return recv(m_sock, (void *)m_buffer, m_bufSize, 0);
}

int UdpSocket::Send()
{
	return sendto(m_sock, (void *)m_buffer, m_bufSize, 0,
			(sockaddr *)&m_fromAddr, sizeof m_fromAddr);
}

void UdpSocket::ClearBuffer()
{
	memset(m_buffer, 0, m_bufSize);
}

char* UdpSocket::GetBuffer() const
{
	return m_buffer;
}

UdpSocket::~UdpSocket()
{
	free(m_buffer);
	close(m_sock); /* Close the socket. */
}

